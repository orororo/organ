﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using FMODUnity;

public class LoadLevelOnAnyKeyPress : MonoBehaviour {

    // Use this for initialization
    public string LevelToLoad;
    public StudioEventEmitter shlurp;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if(Input.anyKeyDown || Input.GetMouseButtonDown(0))
        {
            shlurp.Play();
            SceneManager.LoadScene(LevelToLoad);
        }

	}
}
