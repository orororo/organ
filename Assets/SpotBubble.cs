﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotBubble : MonoBehaviour {

    public Spot SpotOwner;
    private ButtonSW _button;
    public float DestroyTimeAfterPop = 3.0f;

	// Use this for initialization
	void Start () {
        _button = GetComponent<ButtonSW>();
        _button.OnButtonClick += _button_OnButtonClick;

    }

    private void _button_OnButtonClick(ButtonSW obj)
    {
        Pop();
    }

    // Update is called once per frame
    void Update () {
		
	}

    private void Pop()
    {
        this.GetComponent<Animator>().SetTrigger("Pop");
        this.SpotOwner.OnBubblePopped();
        Invoke("DestroyMe", DestroyTimeAfterPop);
    }


    private void DestroyMe()
    {
        GameObject.Destroy(this.gameObject);
    }



}
