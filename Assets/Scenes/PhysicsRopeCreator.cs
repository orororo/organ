﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(ButtonSW))]
[RequireComponent(typeof(SpriteRenderer))]

public class PhysicsRopeCreator : MonoBehaviour
{

    public float RigidbodyMass = 1f;
    public float ColliderRadius = 0.1f;
    public float SpringDampingRatio = 0.7f;
    public float SpringFrequency = 0.3f;
    /*public Vector3 RotationOffset;
    public Vector3 PositionOffset;*/
    public float DistanceBetweenBeets = 1.0f;
    public Vector2 GravityScaleMinMax;
    public float LinearDrag = 1.0f;

    /*public Transform RopeStart;
    public Transform RopeEnd;*/

    protected List<Transform> CopySource;
    protected List<Transform> CopyDestination;
    protected GameObject RigidBodyContainer;

    private Vector3 _prevRopeEnd;
    public bool TwoSided;
    private int _beetCount;
    public LineRenderer Line;
    private GameObject _endBeet;
    private GameObject _previousBeetCreated;

    //public GameObject HeadSprite;
    public SpriteRenderer HeadSprite;

    //private List<Vector2> points = new List<Vector2>();
    private List<GameObject> beets = new List<GameObject>(); // CAVEAT: doesn't contain endbeet
    private ButtonSW _button;
    private bool _isDragging = false;

    void Awake()
    {
        _beetCount = 0;
        _prevRopeEnd = this.transform.position;// RopeStart.position;
        _button = GetComponent<ButtonSW>();
        _button.OnButtonDown += _button_OnButtonDown;
        _isDragging = false;

        RigidBodyContainer = new GameObject("Tentacle");
        CopySource = new List<Transform>();
        CopyDestination = new List<Transform>();

        AddStartBeet();
        AddEndBeet();

        HeadSprite = GetComponent<SpriteRenderer>();
    }

    private void _button_OnButtonDown(ButtonSW obj)
    {
        // start dragging the tentacle
        _isDragging = true;
    }

    private void AddStartBeet()
    {
        var startBeet = new GameObject("beet start");
        startBeet.transform.SetParent(RigidBodyContainer.transform);
        startBeet.transform.position = this.transform.position;// RopeStart.position;

        var childRigidbody = startBeet.gameObject.AddComponent<Rigidbody2D>();
        childRigidbody.isKinematic = true;
        childRigidbody.freezeRotation = true;
        childRigidbody.mass = RigidbodyMass;

        var collider = startBeet.gameObject.AddComponent<CircleCollider2D>();
        collider.offset = Vector3.zero;
        collider.radius = ColliderRadius;

        beets.Add(startBeet);
        _previousBeetCreated = startBeet;
    }

    private void AddEndBeet()
    {
        _endBeet = new GameObject("beet end");
        _endBeet.transform.SetParent(RigidBodyContainer.transform,false);
        _endBeet.transform.position = Vector3.zero;

        var childRigidbody = _endBeet.gameObject.AddComponent<Rigidbody2D>();
        childRigidbody.isKinematic = true;
        childRigidbody.freezeRotation = true;
        childRigidbody.mass = RigidbodyMass;

        var collider = _endBeet.gameObject.AddComponent<CircleCollider2D>();
        collider.offset = Vector3.zero;
        collider.radius = ColliderRadius;
    }

    private void MoveEndBeetToEnd()
    {

    }

    private void AddBeet(Vector3 position)
    {
        var beet = new GameObject("beet" + _beetCount++);
        beet.transform.parent = RigidBodyContainer.transform;
        beet.transform.position = position;
        //rigidbody
        var rb = beet.AddComponent<Rigidbody2D>();
        rb.isKinematic = true;
        rb.freezeRotation = true;
        rb.mass = RigidbodyMass;
        rb.gravityScale = Random.Range(GravityScaleMinMax.x, GravityScaleMinMax.y);
        rb.drag = LinearDrag;

        //collider
        var collider = beet.AddComponent<CircleCollider2D>();
        collider.offset = Vector3.zero;
        collider.radius = ColliderRadius;

        //SpringJoints : one to the prev created beet, one the end beet

        var joint = beet.AddComponent<SpringJoint2D>();
        joint.connectedBody = _previousBeetCreated.GetComponent<Rigidbody2D>();
        joint.autoConfigureDistance = false;
        joint.distance = DistanceBetweenBeets;
        joint.dampingRatio = SpringDampingRatio;
        joint.frequency = SpringFrequency;
      

        var joint2 = beet.AddComponent<SpringJoint2D>();
        joint2.connectedBody = _endBeet.GetComponent<Rigidbody2D>();
        joint2.autoConfigureDistance = false;
        joint2.distance = DistanceBetweenBeets;
        joint2.dampingRatio = SpringDampingRatio;
        joint2.frequency = SpringFrequency;

        // the prev created beet is still hooked up to the end beet but should be hooked up to this newly created beet
        if(_previousBeetCreated.GetComponents<SpringJoint2D>().Length != 0)
            _previousBeetCreated.GetComponents<SpringJoint2D>()[1].connectedBody = rb;

        beets.Add(beet);
        _previousBeetCreated = beet;
    }

    public void Update()
    {
        if (Input.GetMouseButtonUp(0) && _isDragging)
        {
            //_isDragging
            _isDragging = false;

            // do slerpy burp

        }

        if(_isDragging && Input.GetMouseButton(0))
        {
            var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0.0f;
            var delta = mousePos - _prevRopeEnd;
            var deltaX = delta.x;
            //if (delta.sqrMagnitude > DistanceBetweenBeets * DistanceBetweenBeets)
            if(deltaX > DistanceBetweenBeets)
            {
                // we moved the mouse far enough to create new beets
                int nrOfBeetsToCreate = Mathf.FloorToInt(delta.magnitude / DistanceBetweenBeets);
                for (int i = 0; i < nrOfBeetsToCreate; i++)
                {
                    AddBeet(_prevRopeEnd + (i+1) * delta/ nrOfBeetsToCreate);
                }

                _prevRopeEnd = mousePos;
            }
        }

        //if(_isDragging)
        {
            Line.positionCount = beets.Count + 1; //end beet is not in list

            // update line points
            for (int i = 0; i < beets.Count; i++)
            {
                Line.SetPosition(i, beets[i].transform.position);
            }

            var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0.0f;
            if (_isDragging) HeadSprite.transform.position = mousePos;
            if (_isDragging) _endBeet.transform.position = mousePos;

            // end beet
            Line.SetPosition(beets.Count, _endBeet.transform.position);
        }



    }

    public void FixedUpdate()
    {
       
    }

}
