﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnOozeOnClick : MonoBehaviour {

    public GameObject OozePrefab;
    public float zPos = -5.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SpawnOozeAtMousePos()
    {
        var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = zPos;
        var ooze = Instantiate(OozePrefab);
        ooze.transform.position = mousePos;

    }
}
