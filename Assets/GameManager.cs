﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class GameManager : Singleton<GameManager>
{

    public float HookupDistance = 2.0f;
    public bool IsDraggingTentacle = false;
    public bool assignHoleAtRuntime;
    public bool assignTentacleAtRuntime;


    // Use this for initialization
    private void Awake()
    {
        var holes = GameObject.FindObjectsOfType<Hole>();
        if (assignHoleAtRuntime)
        {
            for (int i = 0; i < holes.Length; i++)
            {
                holes[i].HoleIndex = i;
            }
        }


        var tentacles = GameObject.FindObjectsOfType<Tentacle>();
        if (assignTentacleAtRuntime)
        {
            for (int i = 0; i < tentacles.Length; i++)
            {
                tentacles[i].TentacleIndex = i;
            }
        }
    }

    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnHookUp(int tentacleIndex, int holeIndex)
    {
        // Do something with the audiomanager
        if (null != SoundManager.instance)
        {
            SoundManager.instance.Plug(holeIndex, tentacleIndex);
        }
    }

    public void OnUnHook(int tentacleIndex, int holeIndex)
    {
        // Do something with audiomanager
        if (null != SoundManager.instance)
        {
            SoundManager.instance.Unplug(holeIndex);
        }
    }
}
