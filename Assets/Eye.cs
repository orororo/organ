﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eye : MonoBehaviour
{
    public Vector2 WinkDeltaMinMax;
    private Animator _animator;
   


    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        Wink();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Wink()
    {
        _animator.SetTrigger("Wink");
        Invoke("Wink", Random.Range(WinkDeltaMinMax.x, WinkDeltaMinMax.y));
    }
}
