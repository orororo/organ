﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using TMPro;

public class PulsateAlpha : MonoBehaviour {

    public float minAlpha;
    public float maxAlpha;
    public float duration;
	// Use this for initialization
	void Start () {

        Tween.Value(minAlpha, maxAlpha, (x) => OnUpdateAlpha(x), duration, 0.0f, null, Tween.LoopType.PingPong);


	}

    void OnUpdateAlpha(float perc)
    {
        var color = this.GetComponent<TextMeshPro>().color;
        color.a = perc;
        this.GetComponent<TextMeshPro>().color = color;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
