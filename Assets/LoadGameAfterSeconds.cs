﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadGameAfterSeconds : MonoBehaviour
{
    // Start is called before the first frame update
    public float Seconds;
    public string SceneName;

    void Start()
    {
        Invoke("LoadScene", Seconds);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void LoadScene()
    {
        SceneManager.LoadScene(SceneName);
    }
}
