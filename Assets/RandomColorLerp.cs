﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class RandomColorLerp : MonoBehaviour {

    Color _target;
    Color _prevTarget;
    public Vector2 hueMinMax;
    public Vector2 satMinMax;
    public Vector2 valueMinMax;
    public float duration;
    private SpriteRenderer _spriteRend;

	// Use this for initialization
	void Start () {

        _spriteRend = GetComponent<SpriteRenderer>();
        _prevTarget = _spriteRend.color;
        _target = Random.ColorHSV(hueMinMax.x, hueMinMax.y, satMinMax.x, satMinMax.y, valueMinMax.x, valueMinMax.y);

        Tween.Value(0.0f, 1.0f, (x) => OnUpdateAlpha(x), duration, 0.0f, null, Tween.LoopType.Loop, null, ()=> PickNewRandomColor());
    }

    // Update is called once per frame
    void OnUpdateAlpha(float perc) {

        _spriteRend.color = Color.Lerp(_prevTarget, _target, perc);
    }

    void PickNewRandomColor()
    {
        _prevTarget = _target;
        _target = Random.ColorHSV(hueMinMax.x, hueMinMax.y, satMinMax.x, satMinMax.y, valueMinMax.x, valueMinMax.y);
    }
}
