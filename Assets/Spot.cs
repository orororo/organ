﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spot : MonoBehaviour {

    public GameObject SpotBubblePrefab;
    public float zPos = -5.0f;
    private bool HasBubble = false;
    public Vector2 MinMaxTimeBetweenSpawns;

    // Use this for initialization
    void Start () {
        SpawnBubble();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnBubblePopped()
    {
        if (null != SoundManager.instance)
        {
            SoundManager.instance.SpotSound();
        }

        Invoke("SpawnBubble", Random.Range(MinMaxTimeBetweenSpawns.x, MinMaxTimeBetweenSpawns.y));
    }

    public void SpawnBubble()
    {
        var ooze = Instantiate(SpotBubblePrefab);
        ooze.transform.position = this.transform.position;
        ooze.transform.localScale = this.transform.localScale;
        ooze.GetComponent<SpotBubble>().SpotOwner = this;
    }
}
