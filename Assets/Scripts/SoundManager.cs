﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class SoundManager : MonoBehaviour
{
    // SINGLETON
    public static SoundManager instance = null;

    // VARIABLES
    public StudioEventEmitter[] holeEmitters;
    ///<summary>index of the fmod param that say which sound is play</summary>
    public int soundParamIdx = 0;
    public int[] filterParams;
    public StudioEventEmitter shlurp;
    public StudioEventEmitter grabSound;
    public StudioEventEmitter spotSound;


    void Awake ()
    {
        // singleton pattern
        if (null != instance && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this;
    }

    void Start ()
    {
        if (null == holeEmitters || 0 == holeEmitters.Length)
            holeEmitters = GetComponentsInChildren<StudioEventEmitter>();
    }

    public void GenericTentacleSound ()
    {
        if (null == grabSound)
            return;

        grabSound.Play();
    }

    public void SpotSound ()
    {
        if (null == spotSound)
            return;

        spotSound.Play();
    }

    int GetFilterParamIndex (int idx)
    {
        return idx - holeEmitters.Length;
    }

    ///<summary>trigger the hole sound according to tentacle or apply an effect</summary>
    ///<param name="tentacIdx">is not used for the effect hole</param>
    public void Plug (int holeIdx, int tentacIdx)
    {
        if (null != shlurp)
        {
            shlurp.Play();
        }
        // SOUND
        if (holeIdx < holeEmitters.Length)
        {        
            PlayHole(holeIdx, tentacIdx);
            return;
        }

        // EFFECT
        HoleFilter(holeIdx);
    }

    public void Unplug (int holeIdx)
    {
        // SOUND
        if (holeIdx < holeEmitters.Length)
        {        
            StopHole(holeIdx);
            return;
        }

        // EFFECT
        RemoveFilter(holeIdx);
    }

    void StopHole (int holeIdx)
    {
        if (null == holeEmitters || 0 == holeEmitters.Length)
        {
            return;
        }

        holeEmitters[holeIdx].Stop();
    }

    void PlayHole (int holeIdx, int tentacIdx)
    {
        if (null == holeEmitters || 0 == holeEmitters.Length)
        {
            return;
        }

        StudioEventEmitter hole = holeEmitters[holeIdx];
        // set sound according to tentacle
        hole.Params[soundParamIdx].Value = (float)tentacIdx;
        hole.Play();
    }

    void RemoveFilter (int idx)
    {
        if (null == holeEmitters || 0 == holeEmitters.Length)
        {
            return;
        }   

        // get the real index
        int paramIdx = filterParams[GetFilterParamIndex(idx)];
        if (holeEmitters[0].Params.Length <= paramIdx)
        {
            // param doesn't exist
            return;
        }

        // remove for all holes
        for (int i = 0; i < holeEmitters.Length; i++)
        {
            holeEmitters[i].Params[paramIdx].Value = 0f;
        }
    }

    void HoleFilter (int idx)
    {
        if (null == holeEmitters || 0 == holeEmitters.Length)
        {
            return;
        }   

        if (null == filterParams || 0 == filterParams.Length)
        {
            return;
        }

        // get the real index
        int paramIdx = filterParams[GetFilterParamIndex(idx)];
        if (holeEmitters[0].Params.Length <= paramIdx)
        {
            // param doesn't exist
            return;
        }

        // apply to all holes
        for (int i = 0; i < holeEmitters.Length; i++)
        {
            holeEmitters[i].Params[paramIdx].Value = 1f;
        }
    }

    public void LogParams ()
    {
        if (null == holeEmitters || 0 == holeEmitters.Length)
        {
            return;
        }           

        for (int i = 0; i < holeEmitters[0].Params.Length; i++)
        {
            Debug.Log("param " + i + " : " + holeEmitters[0].Params[i].Name);            
        }
    }
}
