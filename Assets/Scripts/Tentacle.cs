﻿using Obi;
using Pixelplacement;
using Pixelplacement.TweenSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tentacle : MonoBehaviour {

    Vector3 _prevMousPos;
    ObiRopeCursor cursor;
    public GameObject Rope;
    public float SegmentLength;
    bool IsFollowing;
    private ObiSolver _solver;
    public Vector2 MinMaxGravityPos;
    public Vector2 MinMaxGravityNeg;
    public Vector2 ChangeGravityMinMaxDelay;
    public bool GravityUp = false;
    public float _restLength;
    public Vector3 _startPosHead;
    public float SnapBackDuration;
    public AnimationCurve SnapBackAnimCurve;
    public float ChangeLengthOffset;
    private Vector3 _posBeforeSnapBack;
    private float _lengthBeforeSnapback;
    public bool EnableSnapBack = false;
    public bool EnableHeadAlignment = false;
    public int TentacleIndex = 0;
    bool IsHooked = false;
    Hole connectedHole;
    public float TentacleOverlapRadius = 0.1f;
    TweenBase _snapBackTween;
    public bool IsLeft = false;
    // Use this for initialization

    private void Awake()
    {
       GetComponent<ButtonSW>().OnButtonDown += FollowMouse_OnButtonDown;
        GetComponent<ButtonSW>().OnButtonUp += Tentacle_OnButtonUp; ;

        _startPosHead = this.transform.position;
        _solver = GameObject.FindObjectOfType<ObiSolver>();
    }

    private void Tentacle_OnButtonUp(ButtonSW obj)
    {
        // if we're above a hole that is not filled, hookup!
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, TentacleOverlapRadius);
        int i = 0;
        while (i < hitColliders.Length)
        {
            var hole = hitColliders[i].GetComponent<Hole>();
            if(hole != null && !hole.isHooked && hole.IsLeft != this.IsLeft)
            {
                hole.HookUp(this);
                this.HookUp(hole);

                if (_snapBackTween != null)
                {
                    _snapBackTween.Stop();
                    _snapBackTween = null;
                }
            }

            i++;
        }

        GameManager.Instance.IsDraggingTentacle = false;


        if (IsFollowing)
        {
            IsFollowing = false;

            if (EnableSnapBack && !IsHooked)
            {
                // do snap back
                _posBeforeSnapBack = this.transform.position;
                _lengthBeforeSnapback = cursor.rope.RestLength;
                //Tween.Position(this.transform, _startPosHead, SnapBackDuration, 0.0f, SnapBackAnimCurve, Tween.LoopType.None, ()=>OnSnapBackComplete());
                _snapBackTween = Tween.Value(0.0f, 1.0f, (x) => UpdateHeadPosition(x), SnapBackDuration, 0.0f, SnapBackAnimCurve, Tween.LoopType.None, () => OnSnapBackHeadComplete());
            }
        }

    }

    public void FollowMouse_OnButtonDown(ButtonSW obj)
    {
        IsFollowing = true;
        EnableHeadAlignment = true;
        _prevMousPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (_snapBackTween != null)
        {
            _snapBackTween.Stop();
            _snapBackTween = null;
        }

        UnHook();

        GameManager.Instance.IsDraggingTentacle = true;
    }

    void Start () {
        cursor = Rope.GetComponentInChildren<ObiRopeCursor>();
        _prevMousPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Invoke("ChangeGravity", Random.Range(ChangeGravityMinMaxDelay.x, ChangeGravityMinMaxDelay.y));
        _restLength = cursor.rope.RestLength;// + ChangeLengthOffset;
    }
	
	// Update is called once per frame
	void Update () {

        var obiRope = Rope.GetComponent<ObiRope>();
      /*  var usedParticles = obiRope.UsedParticles;
        var totalParticles = obiRope.TotalParticles;
        var pooledParticles = obiRope.PooledParticles;
        Vector3[] particles = obiRope.positions;
        Vector3 last = particles[usedParticles - 1];
        Vector3 penultimate = particles[usedParticles - 2];
        Vector3 dir = last - penultimate;*/

        ObiDistanceConstraintBatch dbatch = (ObiDistanceConstraintBatch) obiRope.DistanceConstraints.GetBatches()[0];
        int lastParticle = dbatch.springIndices[(dbatch.ConstraintCount - 1) * 2 + 1];
        int firstParticle = dbatch.springIndices[(dbatch.ConstraintCount - 1) * 2 ];
        Vector3[] particles = obiRope.positions;
        Vector3 last = obiRope.GetParticlePosition(lastParticle);
        Vector3 penultimate = obiRope.GetParticlePosition(firstParticle);
        Vector3 dir = last - penultimate;
        //float zRot = Vector3.Angle(dir.normalized, Vector3.right);
        
        if(EnableHeadAlignment && !IsHooked)
        {
            float zRot = Vector3.Angle(dir.normalized, Vector3.right);
            if (dir.y < 0.0) zRot = -zRot;
            this.transform.eulerAngles = new Vector3(0.0f, 0.0f, zRot);
        }


        if (Input.GetMouseButtonUp(0))
        {
            GameManager.Instance.IsDraggingTentacle = false;
            if (IsFollowing)
            {
                IsFollowing = false;

                if (EnableSnapBack && !IsHooked)
                {
                    // do snap back
                    _posBeforeSnapBack = this.transform.position;
                    _lengthBeforeSnapback = cursor.rope.RestLength;
                    //Tween.Position(this.transform, _startPosHead, SnapBackDuration, 0.0f, SnapBackAnimCurve, Tween.LoopType.None, ()=>OnSnapBackComplete());
                    _snapBackTween = Tween.Value(0.0f, 1.0f, (x) => UpdateHeadPosition(x), SnapBackDuration, 0.0f, SnapBackAnimCurve, Tween.LoopType.None, () => OnSnapBackHeadComplete());
                }
            }

        }

        if(IsFollowing)
        {
            // follow mouse
            var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = -0.6f;
            this.transform.position = mousePos;

            // change length of rope based on  distance to startheadpos
            var delta = mousePos - _prevMousPos;
            //if (deltaX > SegmentLength || deltaX < -SegmentLength)
            {
                //cursor.ChangeLength(cursor.rope.RestLength + deltaX + changeLengthOffset);
                cursor.ChangeLength((mousePos - _startPosHead).magnitude + ChangeLengthOffset);
                _prevMousPos = mousePos;
            }

        }



    }

    void OnSnappingBack()
    {

    }

    void UpdateHeadPosition(float perc)
    {
        this.transform.position = Vector3.Lerp(_posBeforeSnapBack, _startPosHead, perc);
        cursor.ChangeLength(Mathf.Lerp(_lengthBeforeSnapback, _restLength, perc));
    }

    void OnSnapBackHeadComplete()
    {
        if (null != SoundManager.instance)
        {
            //SoundManager.instance.GenericTentacleSound();
        }
    }

    void OnSnapBackComplete()
    {
        //IsFollowing = false;
        //GetComponent<Rigidbody>().isKinematic = false;
        //GetComponent<Rigidbody>().useGravity = true;
    }


    void ChangeGravity()
    {
        GravityUp = !GravityUp;

        //GravityUp = Random.Range(0, 2) > 0 ? true : false;

        float multiplier = 0.0f;
        if(GravityUp)
        {
            multiplier = Random.Range(MinMaxGravityPos.x, MinMaxGravityPos.y);
        }
        else
        {
            multiplier = Random.Range(MinMaxGravityNeg.x, MinMaxGravityNeg.y);
        }

        _solver.parameters.gravity = Vector3.up * multiplier;

        Invoke("ChangeGravity", Random.Range(ChangeGravityMinMaxDelay.x, ChangeGravityMinMaxDelay.y));

        _solver.UpdateParameters();
    }

    public void HookUp(Hole hole)
    {
        IsHooked = true;
        IsFollowing = false;
        GetComponent<SpriteRenderer>().enabled = false;
        transform.position = hole.HoleHookupPoint.position;
        connectedHole = hole;
        GameManager.Instance.OnHookUp(TentacleIndex, hole.HoleIndex);
    }

    public void UnHook()
    {
        Debug.Assert(!IsHooked || (connectedHole != null && IsHooked), "we're hooked but don't have a connected hole?");

        if(connectedHole != null )
        {
            IsHooked = false;
            GetComponent<SpriteRenderer>().enabled = true;

            connectedHole.UnHook();

            GameManager.Instance.OnUnHook(TentacleIndex, connectedHole.HoleIndex);
            connectedHole = null;
        }
        
        if (null != SoundManager.instance)
        {
            SoundManager.instance.GenericTentacleSound();
        }
    }


/*
    private void OnTriggerStay(Collider other)
    {
        // this hole is already filled with a tentacle, ignore
        if (isHooked)
            return;

        var tentacle = other.GetComponent<Tentacle>();
        if (tentacle != null)
        {
            if (TentacleInHole && _mouseDistance < GameManager.Instance.HookupDistance)
            {
                Debug.Log("Hole hookup: " + other.gameObject.name);
                if (tentacle != null)
                {
                    tentacle.transform.position = this.transform.position;
                    tentacle.OnHookUp(this);
                    isHooked = true;
                }
            }
        }
    }*/
}
