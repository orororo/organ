﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookingAt : MonoBehaviour
{
    [SerializeField]
    float offset;

    [SerializeField]
    float maxRelRotation;

    [SerializeField]
    GameObject lookingObject;

    [SerializeField]
    float rotation;

    [SerializeField]
    bool isSmooth;

    [SerializeField]
    float smooth;

    [SerializeField]
    public bool isFront;

    [SerializeField]
    public bool canSee;

    [SerializeField]
    Quaternion originalRot;

    public bool Flippedieflop = false;

    Quaternion targetRot;

    // Use this for initialization
    void Start ()
    {
        originalRot = transform.localRotation;
        targetRot = originalRot;
        //originalRot.x = 0;
        //originalRot.y = 0;

        canSee = true;

    }
	
	// Update is called once per frame
	void Update ()
    {
        /*if (transform.position.x < lookingObject.transform.position.x && isFront)
            canSee = true;
        else if (transform.position.x > lookingObject.transform.position.x && !isFront)
            canSee = true;
        else
            canSee = false;*/

        if (canSee)        
            SetRot();        
        else
            ResetRot();
    }

    void SetRot()
    {

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0.0f;
        Vector3 targetPos = mousePos; //lookingObject.transform.position;
        Vector3 thisPos = transform.position;

       if (isFront)
            targetPos.x =targetPos.x-thisPos.x;
        else
            targetPos.x = thisPos.x - targetPos.x;

        if (isFront)
            targetPos.y = targetPos.y - thisPos.y;
        else
            targetPos.y = targetPos.y - thisPos.y;

        rotation = Mathf.Atan2(targetPos.y, targetPos.x) * Mathf.Rad2Deg;
        rotation += offset;
        if (Flippedieflop)
            rotation = -rotation;

        var originalZrot = originalRot.eulerAngles.z;
        if (originalZrot > 180)
            originalZrot -= 360.0f;

        if (originalZrot < -180)
            originalZrot += 360.0f;

        var tempRot = rotation;
        if (rotation > 180)
            tempRot -= 360.0f;

        if (rotation < -180)
            tempRot += 360.0f;

        if (tempRot < originalZrot + maxRelRotation && tempRot > originalZrot - maxRelRotation)
        {
            targetRot = Quaternion.Euler(0, 0, rotation);
            //if (!isFront)
                //targetRot = Quaternion.Euler(0, 180, rotation);
            /*
            if (isFront)
                transform.rotation = Quaternion.Euler(new Vector3(0, 0, rotation));
            else
                transform.rotation = Quaternion.Euler(new Vector3(0, 180, rotation));
             */
          
        }

        if (isSmooth)
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, Time.deltaTime * smooth);
        else
            transform.rotation = targetRot;
    }

    void ResetRot()
    {
/*
        Quaternion targetRot = Quaternion.Euler(0, 0, 0);
        if (!isFront)
            targetRot = Quaternion.Euler(0, 180, 0);*/

        if (isSmooth)
            transform.rotation = Quaternion.Lerp(transform.rotation, originalRot, Time.deltaTime * smooth);
        else
            transform.rotation = originalRot;
    }
}
