﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;
using UnityEngine.Events;

[System.Serializable]
public class Whatever : UnityEvent<ButtonSW> { }

public class ButtonSW : MonoBehaviour, IPointerClickHandler,
                                  IPointerDownHandler, IPointerEnterHandler,
                                  IPointerUpHandler, IPointerExitHandler
{
    public event Action<ButtonSW> OnButtonClick;
    public event Action<ButtonSW> OnButtonDown;
    public event Action<ButtonSW> OnButtonUp;
    public Whatever OnButtonClick2;

    void Start()
    {
        addEventSystem();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("ButtonSW clicked!");
        if (OnButtonClick != null)
        {
            OnButtonClick(this);
        }

        if(OnButtonClick2 != null)
        {
            OnButtonClick2.Invoke(this);
        }
    }

    public void ClearListeners()
    {
        OnButtonClick = null;
        OnButtonClick2 = null;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("ButtonSW down!");
        if (OnButtonDown != null)
        {
            OnButtonDown(this);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("Mouse Enter!");
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Debug.Log("ButtonSW down!");
        if (OnButtonUp != null)
        {
            OnButtonUp(this);
        }
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("Mouse Exit!");
    }

    //Add Event System to the Camera
    void addEventSystem()
    {
        GameObject eventSystem = null;
        GameObject tempObj = GameObject.Find("EventSystem");
        if (tempObj == null)
        {
            eventSystem = new GameObject("EventSystem");
            eventSystem.AddComponent<EventSystem>();
            eventSystem.AddComponent<StandaloneInputModule>();
        }
        else
        {
            if ((tempObj.GetComponent<EventSystem>()) == null)
            {
                tempObj.AddComponent<EventSystem>();
            }

            if ((tempObj.GetComponent<StandaloneInputModule>()) == null)
            {
                tempObj.AddComponent<StandaloneInputModule>();
            }
        }
    }

}
