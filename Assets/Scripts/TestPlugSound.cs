﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class TestPlugSound : MonoBehaviour
{
    public int holeIdx = 0;
    public KeyCode[] inputs;

    void Update()
    {
        ManageInput();
        LogInput();
    }

    void LogInput ()
    {
        if (null == SoundManager.instance)
            return;

        if (Input.GetButtonDown("Submit"))
        {
            SoundManager.instance.LogParams();
        }
    }

    void ManageInput ()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            StopAll();
        }

        for (int i = 0; i < inputs.Length; i++)
        {
            if (Input.GetKeyDown(inputs[i]))
            {
                Test(i);
            }
        }
    }

    void Test (int input)
    {
        if (null == SoundManager.instance)
            return;

        Debug.Log("test call log");
        SoundManager.instance.Plug(holeIdx, input);
    }

    void StopAll ()
    {
        if (null == SoundManager.instance || null == SoundManager.instance.holeEmitters)
            return;

        int l = SoundManager.instance.holeEmitters.Length;
        if (0 == l)
        {
            return;
        }

        for (int i = 0; i < l; i++)
        {
            SoundManager.instance.Unplug(i);
        }
    }
}
