﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Hole : MonoBehaviour {

    // Use this for initialization

    Animator animator;
    public int HoleIndex;

    bool TentacleInHole = false;
    float _mouseDistance = 0.0f;

    public bool isHooked = false;

    public Transform HoleHookupPoint;

    private ButtonSW _button;

    private Tentacle _connectedTentacle;

    private LookingAt _lookingScript;

    public bool IsLeft = false;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        _button = GetComponent<ButtonSW>();
        _button.OnButtonDown += _button_OnButtonDown;
        _lookingScript = GetComponent<LookingAt>();
    }

    private void _button_OnButtonDown(ButtonSW obj)
    {
        if(isHooked && _connectedTentacle != null)
        {
            _connectedTentacle.FollowMouse_OnButtonDown(null);
        }
    }

    void Start () {
    }
	
	// Update is called once per frame
	void Update () {

        if(!isHooked && GameManager.Instance.IsDraggingTentacle)
        {
            EnableLookAt(true);
            _lookingScript.canSee = true;

            var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0.0f;
            var myPos = transform.position;
            myPos.z = 0.0f;
            _mouseDistance = (mousePos - myPos).magnitude;
            animator.SetFloat("MouseDistance", _mouseDistance);
        }
        else if(!isHooked)
        {
            animator.SetFloat("MouseDistance", 100.0f);
            _lookingScript.canSee = false;
        }
        else
        {
            animator.SetFloat("MouseDistance", 100.0f);
            EnableLookAt(false);
            _lookingScript.canSee = true;
        }

        

       
    }

    void EnableLookAt(bool enable)
    {
        if (enable && isHooked)
            return;

        _lookingScript.enabled = enable;
    }


    public void UnHook()
    {
        isHooked = false;
        animator.SetTrigger("Reset");
        _connectedTentacle = null;
    }

    public void HookUp(Tentacle tentacle)
    {


        isHooked = true;
        animator.SetTrigger("Swallow");
        _connectedTentacle = tentacle;
    }
   
}
