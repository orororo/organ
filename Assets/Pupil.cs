﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pupil : MonoBehaviour
{
    public Transform CenterEye;

    public float factor = 0.25f;
    public float limit = 0.08f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pos.z = 0.0f;
        Vector3 dir = (pos - CenterEye.position) * factor;
        dir = Vector3.ClampMagnitude(dir, limit);
        Vector3 tt = CenterEye.position + dir;
        transform.position = CenterEye.position + dir;
    }
}
